﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace GenericSort
{
    /// <summary>
    /// Main class for the GenericSort project
    /// </summary>
    class Program
    {
        /// <summary>
        /// Entry point to the program
        /// </summary>
        /// <param name="args">Command line arguments</param>
        static void Main(string[] args)
        {
            Sorter<int> sorter1 = new Sorter<int>();
            Sorter<float> sorter2 = new Sorter<float>();
            Sorter<string> sorter3 = new Sorter<string>();

            int[] array1 = new int[] { 1, 5, 4, 3, 2 };
            float[] array2 = new float[] { 3.5F, 2.5F, 4.0F };
            string[] array3 = new string[] { "world", "hello" };

            sorter1.Sort(array1);
            sorter2.Sort(array2);
            sorter3.Sort(array3);

            sorter1.PrintArray(array1);
            sorter2.PrintArray(array2);
            sorter3.PrintArray(array3);
            
            Console.ReadKey();
        }
    }

    /// <summary>
    /// Generic class for sorting an array of any data type for which the CompareTo() method is defined
    /// </summary>
    /// <typeparam name="T">Data type of the array to sort</typeparam>
    class Sorter<T> where T: IComparable
    {
        /// <summary>
        /// Swaps the positions of two items in an array
        /// </summary>
        /// <param name="array">The array containing the items to be swapped</param>
        /// <param name="firstIndex">Index of the first item</param>
        /// <param name="secondIndex">Index of the second item</param>
        public void Swap(T[] array, int firstIndex, int secondIndex)
        {
            T temp = array[firstIndex];
            array[firstIndex] = array[secondIndex];
            array[secondIndex] = temp;
        }

        /// <summary>
        /// Partitions a section of an array so that all items to the left of a pivot value are less than it and all items to the
        /// right are greater than or equal to it
        /// </summary>
        /// <param name="array">The array to be partitioned</param>
        /// <param name="pivot">The pivot value to partition array items about</param>
        /// <param name="leftIndex">Starting index for the section of the array to partition</param>
        /// <param name="rightIndex">Last index for the section of the array to partition</param>
        /// <returns>Returns the index which divides the array into two partitions</returns>
        public int Partition(T[] array, T pivot, int leftIndex, int rightIndex)
        {
            while (leftIndex < rightIndex)
            {
                if (array[leftIndex].CompareTo(pivot) < 0)
                {
                    ++leftIndex;
                    continue;
                }
                else if (array[rightIndex].CompareTo(pivot) >= 0)
                {
                    --rightIndex;
                    continue;
                }
                Swap(array, leftIndex, rightIndex);
                ++leftIndex;
            }

            return leftIndex;
        }

        /// <summary>
        /// Sorts an array of type T recursively, using the Quicksort algorithm
        /// </summary>
        /// <param name="array">The array to be sorted</param>
        /// <param name="startIndex">Starting index for the partition of the array to be sorted</param>
        /// <param name="endIndex">Last index for the partition of the array to be sorted</param>
        public void Quicksort(T[] array, int startIndex, int endIndex)
        {
            if (startIndex < 0 || endIndex >= array.Length || endIndex <= startIndex) return;

            T pivot = array[endIndex];
            int partition = Partition(array, pivot, startIndex, endIndex - 1);

            if (array[partition].CompareTo(pivot) < 0) ++partition;

            Swap(array, partition, endIndex);
            Quicksort(array, startIndex, partition - 1);
            Quicksort(array, partition + 1, endIndex);

            return;
        }

        /// <summary>
        /// Sorts the input array in ascending order
        /// </summary>
        /// <param name="array">The array to be sorted</param>
        public void Sort(T[] array)
        {
            Quicksort(array, 0, array.Length - 1);
        }

        /// <summary>
        /// Prints out the elements of an array of any data type
        /// </summary>
        /// <param name="array">The array to be printed</param>
        public void PrintArray(T[] array)
        {
            foreach (var item in array)
            {
                Console.Write(item.ToString() + " ");
            }
            Console.WriteLine("\n");
        }
    }
}
