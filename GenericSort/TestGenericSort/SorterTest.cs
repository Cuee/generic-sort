﻿using GenericSort;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace TestGenericSort
{
    
    
    /// <summary>
    ///This is a test class for SorterTest and is intended
    ///to contain all SorterTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SorterTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Partition
        ///</summary>
        public void PartitionTestHelper<T>(T[] array)
            where T : IComparable
        {
            Sorter<T> sorter = new Sorter<T>();
            T pivot = array[2];
            int leftIndex = 0;
            int rightIndex = 1;
            int expected = 1;
            int actual;
            actual = sorter.Partition(array, pivot, leftIndex, rightIndex);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        [DeploymentItem("GenericSort.exe")]
        public void PartitionTest()
        {
            PartitionTestHelper<int>(new int[]{ 5, 3, 4 });
            PartitionTestHelper<string>(new string[] { "byte", "ball", "bit" });
        }

        /// <summary>
        ///A test for Sort
        ///</summary>
        public void SortTestHelper()
        {
            Sorter<int> sorter = new Sorter<int>();
            Sorter<string> sorter2 = new Sorter<string>();
            Sorter<float> sorter3 = new Sorter<float>();

            int[] array = { 5, 3, 4 };
            int[] expected = { 3, 4, 5 };
            sorter.Sort(array);
            for (int i = 0; i < expected.Length; i++)
            {
                Assert.AreEqual(expected[i], array[i]);
            }

            string[] array2 = { "byte", "ball", "bit" };
            string[] expected2 = { "ball", "bit", "byte" };
            sorter2.Sort(array2);
            for (int i = 0; i < expected2.Length; i++)
            {
                Assert.AreEqual(expected2[i], array2[i]);
            }

            float[] array3 = { 5F, 3F, 4F };
            float[] expected3 = { 3F, 4F, 5F };
            sorter3.Sort(array3);
            for (int i = 0; i < expected3.Length; i++)
            {
                Assert.AreEqual(expected3[i], array3[i]);
            }
        }

        [TestMethod()]
        public void SortTest()
        {
            SortTestHelper();
        }

        /// <summary>
        ///A test for Swap
        ///</summary>
        public void SwapTestHelper()
        {
            Sorter<int> sorter = new Sorter<int>();
            int[] array = new int[] { 5, 3, 4 };
            int firstIndex = 0;
            int secondIndex = 2;
            int[] expected = new int[] { 4, 3, 5 };
            sorter.Swap(array, firstIndex, secondIndex);
            for (int i = 0; i < expected.Length; i++)
            {
                Assert.AreEqual(expected[i], array[i]);
            }
        }

        [TestMethod()]
        [DeploymentItem("GenericSort.exe")]
        public void SwapTest()
        {
            SwapTestHelper();
        }
    }
}
