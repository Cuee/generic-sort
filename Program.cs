using System;
using System.Collections.Generic;

namespace GenericSort
{
     /// <summary>
	 /// This class performs <c>Generic sorting</c>.
	 /// </summary>
	 /// <typeparam name = "T"> A type that inherits from the IComparable interface. </typeparam>
	 class Item<T> where T : IComparable<T> 
	 {
       /// <summary>
	   /// The returned list of unsorted elements.
	   /// </summary>
       public List<T> unsortedList = new List<T>();
       /// <summary>
	   /// The returned list of sorted elements.
	   /// </summary>
       public List<T> sortedList =  new List<T>();

      /// <summary>
	   /// Accepts the set of elements to be sorted.
	   /// </summary>
	   /// <param name = "unsortedList"> An unsorted list of Type T.</param>
      public void AcceptInput(List<T> unsortedList)
      {
      	    string Input;
      	    T elementsOfArray;
      	    string[] input;
         	Console.WriteLine("Enter the set of elements to be sorted: ");
            Input = Console.ReadLine();
         	input = Input.Split(',');
            foreach (var inputt in input)
            {
            	elementsOfArray = (T)Convert.ChangeType(inputt, typeof (T));
                unsortedList.Add(elementsOfArray);
           }
            for (int elements = 0; elements < unsortedList.Count; elements++)
            {

            Console.WriteLine("The unsorted list is: " + unsortedList[elements] );

           }

           return;
       }

      ///<summary>
	   ///Recursively performs <c>Bubble sort</c> on elements of Type T.
	   ///</summary>
	   ///<remarks>
	   ///<para>This sort is done by comparing two adjacent elements in an array and placing the lesser one at the left.</para>
	   ///<para>This operation can be performed on any type T.</para>
	   ///</remarks>
	   /// <param name = "unsortedList"> A list of Type T to be sorted.</param>
      public void BubbleSort(List <T> unsortedList)
      {

       	int loopCount = 0;
       	bool flag = true;
       	T[] array = unsortedList.ToArray();    	
      	
      	for (int i = 0; i < array.Length ; i++)
      	{
      		flag = true;
        	for (int j = 0; j < array.Length - 1; j++)
        	{
        		if (array[j].CompareTo(array[j+1]) > 0 )
        		{
        			T temp = array[0];
        			temp = array[j+1];
        			array[j+1] = array[j];
        			array[j] = temp;
        			flag = false;
        	}
        	loopCount++;
        }
        if (flag)
        {
        	break;
        }
    }
    for (int i = 0;i < array.Length; i++){
        	
        	sortedList.Add(array[i]);
        }

        return;
    }
   

       ///<summary>
	   ///Prints out a list of sorted elements of type T.
	   ///</summary>
       /// <param name = "sortedList"> A sorted list of Type T.</param>
    public void PrintList(List <T> sortedList)
        {
            foreach (var item in sortedList)
            { 
            	Console.WriteLine("------------------------------------");
        	    Console.WriteLine("The sorted list is: " + item + "");
            }

            return;
           
        }

       
}

class Program
    {
    	///<summary>
	   ///Main entry point of the GenericSort program.
	   ///</summary>
        static void Main(string[] args) 
        {
        	Item<string> item = new Item<string>();
          item.AcceptInput(item.unsortedList);
          item.BubbleSort(item.unsortedList);
          item.PrintList(item.sortedList);
        	Console.ReadKey();
        }
    }
}
	